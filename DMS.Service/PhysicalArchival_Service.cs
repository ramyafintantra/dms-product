﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DMS.Data;
using DMS.Model;

namespace DMS.Service
{
    public class PhysicalArchival_Service
    {
        PhysicalArchival_Data dataObj = new PhysicalArchival_Data();
        public DataSet GetIndexedDocuments(int? DeptID1, int? Unit1, int? Dgroup1, int? Dname1)
        {
            return dataObj.GetIndexedDocuments(DeptID1, Unit1, Dgroup1, Dname1);
        }

        public DataSet Initialvalues(int? deptid, int? unit, int? docgroup, int? docname, string Aaction, int? attribgid)
        {
            return dataObj.Initialvalues(deptid, unit, docgroup, docname, Aaction, attribgid);
        }

        public int SaveStorageAttributes(PhysicalArchival_Model modelObj, List<PhysicalArchival_Model> ModelObjList)
        {
            int Result;
            try
            {
                Result = dataObj.SaveStorageAttributes(modelObj, ModelObjList);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public DataTable checkvalidone(int? GridID)
        {
            return dataObj.checkvalidone(GridID);
        }
    }
}
