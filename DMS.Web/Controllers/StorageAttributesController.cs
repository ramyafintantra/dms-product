﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Web.Filters;
using DMS.Service;
using System.Data;
using DMS.Model;
using Newtonsoft.Json;

//vadivu code
namespace DMS.Web.Controllers
{

    [UserAuntheication]
    public class StorageAttributesController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ConfigureAttributesController));  //Declaring Log4Net
        StorageAttributes_Service Storageobjsrv = new StorageAttributes_Service();
        //StorageAttributes_Service Strobjsrv = new StorageAttributes_Service();
        // GET: StorageAttributes
        public ActionResult StorageAttributes()
        {
            return View();
        }

        //Save storage attribute. 
        [HttpPost]
        public ActionResult Save(int DeptID, int UnitID, int DgroupID, int DNameID, string[] attributes1, string[] attributes2, string[] attributes3, string[] attributes4, string[] attributes5)
        {
            int Result = 0;
            try
            {
                int UserID = Convert.ToInt32(Session["Emp_Id"].ToString());
                string[] attrnameval = attributes1[0].ToString().Split(',');
                string[] attrtypeval = attributes2[0].ToString().Split(',');
                string[] attrlenval = attributes3[0].ToString().Split(',');
                string[] attrmandatoryval = attributes4[0].ToString().Split(',');
                string[] Storage_orderid = attributes5[0].ToString().Split(',');
                DataSet ds = new DataSet();
                for (int i = 0; i < attrnameval.Length; i++)
                {
                    string Len = attrlenval[i].ToString();
                    if (Len == "")
                    {
                        Len = "0";
                    }
                    ds = Storageobjsrv.SaveStorageAttri(attrnameval[i].ToString(), Convert.ToInt16(Len), attrtypeval[i].ToString(), attrmandatoryval[i].ToString(), Convert.ToInt16(Storage_orderid[i].ToString()), DeptID, UnitID, DgroupID, DNameID, UserID);

                }
                Result = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());

                if (Result == 1)
                {
                    ds = Storageobjsrv.saveStorageAtt(DeptID, UnitID, DgroupID, DNameID, UserID);
                    ViewBag.Message = "File uploaded successfully";

                }
            }

            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            // return View();
            return Json(new { success = Result, JsonRequestBehavior.AllowGet });
        }

        //chexksavestorageattributes
        [HttpPost]
        public JsonResult CheckStorageattribute(int? DeptID, int? UnitID, int? DgroupID, int? DNameID)
        {
            int Result = 0;
            try
            {

                DataSet ds = new DataSet();
                ds = Storageobjsrv.CheckSaveStorageattrib(DeptID, UnitID, DgroupID, DNameID);
                Result = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        //04-04-2019 Viewpartialviewedit

        //#region View and Addsameas

        public ActionResult ViewPartialViewEdit(int sameasdocnameid)
        {
            ViewBag.samednameid = sameasdocnameid;
            return PartialView("StorageAttributePartialView"); // for StorageAttributes
        }

        public JsonResult GetSameasattributesdata(int sameasdocnameid)
        {
            int count = 0;
            int Docnameid = Convert.ToInt32(sameasdocnameid);
            string Data1 = "";
            try
            {
                DataTable dt = new DataTable();
                dt = Storageobjsrv.GSameasAttributeValues(Docnameid);
                if (dt.Rows.Count > 0)
                {
                    count = dt.Rows.Count;
                    Data1 = JsonConvert.SerializeObject(dt);
                }
                return Json(new { count, Data1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json(count, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetSameasAttributeplusdata(int sameasdocnameid)
        {
            int count = 0;
            int Docnameid = Convert.ToInt32(sameasdocnameid);
            string Data1 = "";
            try
            {
                DataTable dt = new DataTable();
                dt = Storageobjsrv.GetSameasAttributeplusdata(Docnameid);
                if (dt.Rows.Count > 0)
                {
                    count = dt.Rows.Count;
                    Data1 = JsonConvert.SerializeObject(dt);
                }
                return Json(new { count, Data1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json(count, JsonRequestBehavior.AllowGet);
            }
        }

        //#endregion

    }
}