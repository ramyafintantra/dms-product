﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Web.Filters;
using DMS.Model;
using DMS.Service;
using System.Data;
using Newtonsoft.Json;

//prakash code
namespace DMS.Web.Controllers
{
    [UserAuntheication]
    public class PhysicalArchivalController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(GetAllDocumentsController));
        PhysicalArchival_Service serviceObj = new PhysicalArchival_Service();
        PhysicalArchival_Model modelObj = new PhysicalArchival_Model();
        // GET: PhysicalArchival
        public ActionResult PhysicalArchival()
        {
            return View();
        }

        public ActionResult GetIndexedDocuments(int? DeptID1, int? Unit1, int? Dgroup1, int? Dname1)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                ds = serviceObj.GetIndexedDocuments(DeptID1, Unit1, Dgroup1, Dname1);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    dt1 = ds.Tables[1];
                }
                string Data1, Data2;
                Data1 = JsonConvert.SerializeObject(dt);
                Data2 = JsonConvert.SerializeObject(dt1);
                return Json(new { Data1, Data2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

        public ActionResult ShowPopUpWindow(int? deptid, int? unit, int? docgroup, int? docname, string Aaction, int? attribgid)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                ds = serviceObj.Initialvalues(deptid, unit, docgroup, docname, Aaction, attribgid);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    Session["dt"] = dt;
                    foreach (DataRow dr in dt.Rows)
                    {
                        PhysicalArchival_Model depts = new PhysicalArchival_Model();
                        depts.Satrname = dr["Satr_Name"].ToString();
                        //depts.Satrid = dr["Satr_Id"].ToString();
                        depts.Satrtype = dr["Satr_Type"].ToString();
                        depts.Satrlen = dr["Satr_Length"].ToString();
                        depts.SatrMand = dr["Satr_Mandotry"].ToString();
                        if (Aaction == "Edit" || Aaction == "Delete")
                        {
                            depts.Satrdesc = dr["Sattribdtl_Description"].ToString();
                        }
                        else
                        {
                            depts.Satrdesc = null;
                        }
                        modelObj.dept.Add(depts);
                    }
                    //modelObj.SatrCount = Convert.ToInt32(ds.Tables[1].Rows[0]["count"].ToString());
                }
                return PartialView("DynamicStorageAttributes", modelObj);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return PartialView("DynamicStorageAttributes", modelObj);
            }
        }

        [HttpPost]
        public ActionResult Save(string[] form, PhysicalArchival_Model modelObj, string[] form1)
        {
            int Result = 0;
            try
            {
                List<PhysicalArchival_Model> ModelObjList = new List<PhysicalArchival_Model>();
                int AtrCount = modelObj.SatrCount;
                modelObj.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                DataTable dt = Session["dt"] as DataTable;

                for (int j = 0; j < form1.Length; j++)
                {
                    for (int i = 0; i < form.Length; i++)
                    {
                        PhysicalArchival_Model ModelObject = new PhysicalArchival_Model();
                        ModelObject.Satrname = dt.Rows[i]["Satr_Name"].ToString();
                        ModelObject.Satrtype = dt.Rows[i]["Satr_Type"].ToString();
                        ModelObject.Satrlen = dt.Rows[i]["Satr_Length"].ToString();
                        ModelObject.SatrMand = dt.Rows[i]["Satr_Mandotry"].ToString();
                        //ModelObject.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                        ModelObject.Atrgid = Convert.ToInt32(form1[j].ToString());
                        ModelObject.DynamicVal = form[i];
                        ModelObjList.Add(ModelObject);
                    }
                }
                //passing model object and model list to service class
                Result = serviceObj.SaveStorageAttributes(modelObj, ModelObjList);
                if (Result == 1)
                {
                    if (modelObj.action == "Edit")
                    {
                        Result = 2;
                    }
                    if (modelObj.action == "Delete")
                    {
                        Result = 3;
                    }
                }
                return Json(new { status = Result });
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return Json(new { status = Result });
            }
        }

        public ActionResult checkvalidone(int? GridID)
        {
            DataTable dt = new DataTable();
            string Result = "";
            try
            {
                dt = serviceObj.checkvalidone(GridID);
                Result = dt.Rows[0][0].ToString();
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View();
            }
        }

    }
}