﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DMS.Service;
using DMS.Model;

namespace DMS.Web.Controllers
{
    public class _LayoutController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(_LayoutController));  //Declaring Log4Net 
        EmpDetails_Model Objmodel = new EmpDetails_Model();
        // GET: Shared
        public ActionResult _Layout()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EmpProfilePartialView()
        {
            // string emp_name = Session["Employee_Name"].ToString();
            int emp_id = Convert.ToInt32(Session["Emp_Id"].ToString());
            EmpProfileServ Objservice = new EmpProfileServ();
            try
            {

                DataTable dt = new DataTable();
                dt = Objservice.EmpProfileSer(emp_id);
                if (dt.Rows.Count > 0)
                {

                    ViewBag.EmpCode = dt.Rows[0]["Emp_Code"].ToString();
                    ViewBag.EmpName = dt.Rows[0]["Emp_Name"].ToString();
                    ViewBag.Grade = dt.Rows[0]["Grade_Name"].ToString();
                    ViewBag.EmpTitle = dt.Rows[0]["Title_Name"].ToString();
                    ViewBag.Dept = dt.Rows[0]["Dept_Name"].ToString();
                    ViewBag.Unit = dt.Rows[0]["Unit_Name"].ToString();
                    ViewBag.EmpDOJ = dt.Rows[0]["Emp_DOJ"].ToString();
                    ViewBag.Address = dt.Rows[0]["Emp_Address"].ToString();
                    ViewBag.City = dt.Rows[0]["City_Name"].ToString();
                    ViewBag.Pin = dt.Rows[0]["Pin_Code"].ToString();
                    ViewBag.State = dt.Rows[0]["State_Name"].ToString();
                    ViewBag.Region = dt.Rows[0]["Region_Name"].ToString();
                    ViewBag.EmpMobileNo = dt.Rows[0]["Emp_MobileNo"].ToString();
                    ViewBag.EmpMailId = dt.Rows[0]["Emp_EmailId"].ToString();
                }
                else
                {
                    ViewBag.grd_lst = null;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return PartialView("EmpProfilePartialView");
            //return PartialView("~/Views/Shared/EmpProfilePartialView.cshtml", Objmodel);
        }

 
    }
}