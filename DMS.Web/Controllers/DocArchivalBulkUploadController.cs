﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DMS.Web.Filters;
using DMS.Model;
using DMS.Service;
using System.Data;
using System.Configuration;
using ClosedXML.Excel;
using System.IO;
using System.Data.OleDb;
using System.Text.RegularExpressions;

namespace DMS.Web.Controllers
{
    [UserAuntheication]
    public class DocArchivalBulkUploadController : Controller
    {
        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(DocArchivalBulkUploadController));//Declaring Log4Net.
        DocArchivalBulkUpload_Service bulkserviceObj = new DocArchivalBulkUpload_Service();//Creating DocArchivalBulkUpload object.
        DocArchival_Model ModelObj = new DocArchival_Model(); //Creating DocArchival model object
        DocArchival_Service ServiceObj = new DocArchival_Service();  //Creating DocArchival Service Object
        [HttpGet]
        public ActionResult DocArchivalBulkUpload()// GET: DocArchivalBulkUpload
        {
            Session["fileid"] = 0;
            Session["Singlefile"] = null;
            Session["Multiplefile"] = null;
            return View();
        }
        //Bug_Id-9 Validation issue is fixed.
        [HttpPost]
        public ActionResult Validation(int Department, int Unit, int Docgroup, int Docname, string useracceptance) //Validating Documents file is already exist.
        {
            HttpPostedFileBase flexcelfile = Session["Singlefile"] as HttpPostedFileBase; //Attribute excel files.
            HttpPostedFileBase[] flbulkfiles = Session["Multiplefile"] as HttpPostedFileBase[]; //user selected file.
            int message;
            string errormessage = "";
            try
            {
                DataSet ds = new DataSet(); //Creating dataset.
                if (flexcelfile != null && flbulkfiles != null)
                {
                    int userid = Convert.ToInt32(Session["Emp_Id"].ToString());
                    ModelObj.DeptId = Department;
                    ModelObj.UnitId = Unit;
                    ModelObj.CatgId = Docgroup;
                    ModelObj.SubCatgId = Docname;
                    ModelObj.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                    DataTable dt = new DataTable(); //Creating datatable.
                    dt = bulkserviceObj.getattributes(ModelObj);//Get dynamic Attributes for selected dropdown combination.
                    int rowcount = dt.Rows.Count;
                    dt.Rows.Add(rowcount + 1, "File Name", 1000, "Alpha Numeric", "Y", 0);//adding new rows to datatable.
                    string _checkatrribute = dt.Rows[0][0].ToString();
                    if (_checkatrribute != "attributesnotfound")
                    {
                        if (flexcelfile.ContentLength > 0)
                        {
                            string fileExtension = System.IO.Path.GetExtension(flexcelfile.FileName);//get file selected file extension.
                            string filePath = ConfigurationManager.AppSettings["Path2"].ToString();
                            var InputFileName = Path.GetFileName(flexcelfile.FileName);
                            var ServerSavePath = Path.Combine(Path.Combine(filePath, Path.GetFileName(flexcelfile.FileName)));
                            string fileLocation = ServerSavePath;
                            //Delete the file if already exist in same name.
                            if (System.IO.File.Exists(fileLocation))
                            {
                                System.IO.File.Delete(fileLocation);
                            }
                            //Save file to server folder.  
                            flexcelfile.SaveAs(ServerSavePath);
                            string excelConnectionString = string.Empty;
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=PathToExcelFileWithExtension;Extended Properties='Excel 8.0;HDR=YES'";

                            if (fileExtension == ".xls")
                            {
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            }
                            else if (fileExtension == ".xlsx")
                            {
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                            }
                            OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);//connection for excel.
                            excelConnection.Close();//excel connection close.
                            excelConnection.Open();//excel connection open.
                            DataTable dtexcel = new DataTable();//datatable object.
                            dtexcel = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dtexcel == null)
                            {
                                return null;
                            }
                            String[] excelSheets = new String[dtexcel.Rows.Count];
                            int t = 0;
                            //excel data saves in temp file here.
                            foreach (DataRow row in dtexcel.Rows)
                            {
                                excelSheets[t] = row["TABLE_NAME"].ToString();
                                t++;
                            }
                            OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);//connection for excel.
                            int excel = excelSheets.Count();
                            if (excel == 0)
                            {
                                errormessage = "Please select valid file..!";
                                excelConnection.Close();
                            }
                            else
                            {
                                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                                {
                                    dataAdapter.Fill(ds);
                                }
                                DataTable dtexceldata = new DataTable();
                                if (ds.Tables.Count > 0)
                                {
                                    dtexceldata = ds.Tables[0]; //Excel Data
                                    //code for removing invalid empty rows from datatable.
                                    dtexceldata = dtexceldata.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field =>
                                    field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();

                                    //if ((dtexceldata != null) && (dtexceldata.Rows != null) && (dtexceldata.Rows.Count > 0))
                                    //{
                                    //    List<DataRow> removeRowIndex = new List<DataRow>();
                                    //    int RowCounter = 0;
                                    //    foreach (DataRow dRow in dtexceldata.Rows)
                                    //    {
                                    //        for (int index = 0; index < dtexceldata.Columns.Count; index++)
                                    //        {
                                    //            if (dRow[index] == DBNull.Value)
                                    //            {
                                    //                removeRowIndex.Add(dRow);
                                    //                break;
                                    //            }
                                    //            else if (string.IsNullOrEmpty(dRow[index].ToString().Trim()))
                                    //            {
                                    //                removeRowIndex.Add(dRow);
                                    //                break;
                                    //            }
                                    //        }
                                    //        RowCounter++;
                                    //    }
                                    //    // Remove all blank of in-valid rows
                                    //    foreach (System.Data.DataRow rowIndex in removeRowIndex)
                                    //    {
                                    //        dtexceldata.Rows.Remove(rowIndex);
                                    //    }
                                    //}
                                }

                                if (useracceptance == "allow")
                                {
                                    message = savefiles(flbulkfiles, ModelObj, dtexceldata, flexcelfile, userid);//Save Documents
                                    if (message == 1)
                                    {
                                        errormessage = flbulkfiles.Count() + " " + "Files Uploaded and Indexed Scuccessfully..!!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        errormessage = "Uploading Failed..!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                }

                                if (dtexceldata.Rows.Count == 0)
                                {
                                    errormessage = "Mandatory fields are empty in Excel file..!";
                                    excelConnection.Close();
                                    return Json(errormessage, JsonRequestBehavior.AllowGet);
                                }

                                string _excelduplicates = "N";
                                DataTable dtfindduplicate = new DataTable();
                                dtfindduplicate.Rows.Clear();
                                //code for remove duplicate rows to datatable.
                                var UniqueRows = dtexceldata.AsEnumerable().Distinct(DataRowComparer.Default);
                                dtfindduplicate = UniqueRows.CopyToDataTable();
                                if (dtexceldata.Rows.Count.ToString() != dtfindduplicate.Rows.Count.ToString())
                                {
                                    _excelduplicates = "Y";
                                    errormessage = "Duplicate rows occured do you want continue.!";
                                    excelConnection.Close();
                                    return Json(errormessage, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    _excelduplicates = "N";

                                }

                                string ismatched = "N";
                                string isfilematched = "N";

                                //compare columns.
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    string colmunname = dt.Rows[i]["Atr_Name"].ToString().ToUpper();
                                    for (int j = 0; j < dtexceldata.Columns.Count; j++)
                                    {
                                        if (colmunname == dtexceldata.Columns[j].ToString().ToUpper())
                                        {
                                            ismatched = "Y";
                                            break;
                                        }
                                        else
                                        {
                                            ismatched = "N";
                                        }
                                    }
                                    if (ismatched == "N")
                                    {
                                        errormessage = "Attributes name does not matched in excel.!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                }

                                //compare selected files are matched to be mentioned in excel sheet.
                                for (int n = 0; n < dtexceldata.Rows.Count; n++)
                                {
                                    string excelfilename = dtexceldata.Rows[n]["File Name"].ToString().ToUpper();

                                    foreach (HttpPostedFileBase file in flbulkfiles)
                                    {
                                        if (file != null)
                                        {
                                            string physicalfilename = file.FileName;
                                            if (excelfilename == physicalfilename.ToUpper())
                                            {
                                                isfilematched = "Y";
                                                break;
                                            }
                                            else
                                            {
                                                isfilematched = "N";
                                            }
                                        }
                                    }
                                    if (isfilematched == "N")
                                    {
                                        errormessage = "Choosen files does not matched mentioned in excel.!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                }

                                //code for validate excel data's.
                                string validationflag = "N";
                                for (int p = 0; p < dt.Rows.Count; p++)
                                {
                                    string dttype = dt.Rows[p]["Atr_Type"].ToString();
                                    string dtname = dt.Rows[p]["Atr_Name"].ToString().ToUpper();
                                    int dtlenght = Convert.ToInt32(dt.Rows[p]["Atr_Length"].ToString());
                                    string dtmandotry = (string)dt.Rows[p]["Atr_Mandotry"].ToString().ToUpper();
                                    int dtlov = Convert.ToInt32(dt.Rows[p]["Lov_Id"].ToString());

                                    for (int q = 0; q < dtexceldata.Columns.Count; q++)
                                    {
                                        string dtexcelname = dtexceldata.Columns[q].ToString().ToUpper();
                                        if (dtname == dtexcelname)
                                        {
                                            for (int j = 0; j < dtexceldata.Rows.Count; j++)
                                            {
                                                string userdata = dtexceldata.Rows[j][dtexcelname].ToString();

                                                if (dttype == "Alpha Numeric")//Validate given data is Alpha Numeric.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            int userdatalenght = userdata.Length;
                                                            if (dtlenght >= userdatalenght)
                                                            {
                                                                validationflag = "Y";
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                                if (dttype == "Numeric")//Validate given data is Numeric.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            int num;
                                                            bool isNumeric = int.TryParse(userdata, out num);
                                                            if (isNumeric == true)
                                                            {
                                                                validationflag = "Y";
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                                if (dttype == "Amount")//Validate given data is Double.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            var regex = new Regex(@"^[0-9]*(?:\.[0-9]*)?$");
                                                            bool isFloat = regex.IsMatch(userdata);
                                                            if (isFloat == true)
                                                            {
                                                                validationflag = "Y";
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                                if (dttype == "Datetime")//Validate given data is Datetime.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            DateTime date;
                                                            if (DateTime.TryParse(userdata, out date))
                                                            {
                                                                validationflag = "Y";
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                                if (dttype == "Lov Name")//Validate given data is LOV.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            string LOV_Validation = bulkserviceObj.validateLOV(dtlov, userdata);
                                                            if (LOV_Validation == "valid")
                                                            {
                                                                validationflag = "Y";
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else 
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                                if (dttype == "Logical")//Validate given data is Logical.
                                                {
                                                    if (dtmandotry == "Y")
                                                    {
                                                        if (userdata != "" && userdata != null)
                                                        {
                                                            if (userdata.ToUpper() == "Y" || userdata.ToUpper() == "N")
                                                            {
                                                                validationflag = "Y";
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                validationflag = "N";
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            validationflag = "N";
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        validationflag = "Y";
                                                        continue;
                                                    }
                                                }

                                            }
                                            if (validationflag == "N")
                                            {
                                                break;
                                            }
                                        }
                                        //if (validationflag == "N")
                                        //{
                                        //    break;
                                        //}
                                    }
                                    if (validationflag == "N")
                                    {
                                        break;
                                    }
                                }
                                if (validationflag == "N")
                                {
                                    errormessage = "Data validation error occured..!";
                                    excelConnection.Close();
                                    return Json(errormessage, JsonRequestBehavior.AllowGet);
                                }
                                string attributevalidationflag = bulkserviceObj.validateattribute(dtexceldata, ModelObj);//Check attributes is already exist.
                                if (attributevalidationflag == "duplicate not found")
                                {
                                    //perform scanning and indexing operation.
                                    message = savefiles(flbulkfiles, ModelObj, dtexceldata, flexcelfile, userid);
                                    if (message == 1)
                                    {
                                        errormessage = flbulkfiles.Count() + " " + "Files Uploaded and Indexed Scuccessfully..!!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        errormessage = "Uploading Failed..!";
                                        excelConnection.Close();
                                        return Json(errormessage, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    errormessage = "DuplicateAttributesFountInDB";
                                    excelConnection.Close();
                                    return Json(errormessage, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }
                    }
                    else
                    {
                        //wriiting code for without attributes.
                    }
                }
                else
                {
                    errormessage = "You have not specified a file..!";
                }
                return Json(errormessage, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                errormessage = "Invalid excel file..!";

                return Json(errormessage, JsonRequestBehavior.AllowGet);
                logger.Error(ex.ToString());
            }
        }

        //Save Documents.
        public int savefiles(HttpPostedFileBase[] flbulkfiles, DocArchival_Model ModelObj, DataTable dtexceldata, HttpPostedFileBase flexcelfile, int userid)
        {
            int message = 0;
            foreach (HttpPostedFileBase file in flbulkfiles)
            {
                if (file != null)
                {
                    int result = bulkserviceObj.SaveSingleFile(ModelObj, file);
                    if (result > 0)
                    {
                        message = getindexedinialatributes(result, dtexceldata, file.FileName);
                    }
                    else
                    {
                        //ViewBag.Message = "Upload Failed.!";
                        //return View("DocArchivalBulkUpload");
                    }
                }
            }
            if (message == 1)
            {
                int msg = bulkserviceObj.saveexclefile(flexcelfile, userid);
                //if (msg == 1)
                //{
                //    ViewBag.Message = flbulkfiles.Count() + " " + "Files Uploaded and Indexed Scuccessfully..!!";
                //}
            }
            else
            {
                //ViewBag.Message = "Uploading Failed..!!";
            }
            return message;
        }

        [HttpPost]
        public void flexcelfile_onchange(HttpPostedFileBase flexcelfile)//onchange method.
        {
            Session["Singlefile"] = null;
            Session["Singlefile"] = flexcelfile;
            //foreach (string file in Request.Files)
            //{
            //    var fileContent = Request.Files[file];

            //    if (fileContent != null && fileContent.ContentLength > 0)
            //    {
            //        HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
            //        Session["Singlefile"] = hpf;
            //    }
            //}
        }

        [HttpPost]
        public void flbulkfiles_onchange(HttpPostedFileBase[] flbulkfiles) //onchange method.
        {
            Session["Multiplefile"] = null;
            Session["Multiplefile"] = flbulkfiles;
        }

        public int getindexedinialatributes(int id, DataTable dtx, string psotedfilename)//perform indexing activity.
        {
            SetDocAttributes_Model deptsModel = new SetDocAttributes_Model();//Creating SetDocAttributes model object.
            SetDocAttributes_Service ServiceObj = new SetDocAttributes_Service();//Creating SetDocAttributes service object.
            List<SetDocAttributes_Model> ModelObjList1 = new List<SetDocAttributes_Model>();//Creating list. 
            try
            {
                DataSet ds = new DataSet();
                ds = ServiceObj.InitValues(id);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    deptsModel.FileName = ds.Tables[0].Rows[0][1].ToString();
                    deptsModel.FileType = ds.Tables[0].Rows[0][2].ToString();
                    deptsModel.HideDocArchId = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                    ViewBag.FilePath = ds.Tables[0].Rows[0][3].ToString();
                    deptsModel.Filepathfrom = ds.Tables[0].Rows[0][3].ToString();
                    deptsModel.VersionDate = DateTime.Today;
                    deptsModel.VersionName = "0.1";
                    deptsModel.Signature = "Test";
                    deptsModel.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    deptsModel.DeptName = ds.Tables[1].Rows[0][3].ToString();
                    deptsModel.UnitName = ds.Tables[1].Rows[0][5].ToString();
                    deptsModel.CateName = ds.Tables[1].Rows[0][7].ToString();
                    deptsModel.SubCateName = ds.Tables[1].Rows[0][9].ToString();
                }
                DataTable dt = new DataTable();
                dt = ds.Tables[3];

                string category = deptsModel.CateName;
                string subcategory = deptsModel.SubCateName;

                string filePath = ConfigurationManager.AppSettings["Path"].ToString();

                string Catename = filePath + category;
                string SubCatename = subcategory;

                string ExistingfilePath = deptsModel.Filepathfrom;
                deptsModel.Filepathto = filePath + category + "\\" + SubCatename + "\\" + deptsModel.FileName;

                bool loggingDirectoryExists = false;

                DirectoryInfo ObjDirectoryInfo = new DirectoryInfo(Catename);

                if (ObjDirectoryInfo.Exists)
                {
                    loggingDirectoryExists = true;
                    DirectoryInfo ObjSubDirectoryInfo = new DirectoryInfo(SubCatename);
                    if (ObjSubDirectoryInfo.Exists)
                    {
                        loggingDirectoryExists = true;
                    }
                    else
                    {
                        ObjSubDirectoryInfo = ObjDirectoryInfo.CreateSubdirectory(SubCatename);
                    }
                }
                else
                {
                    Directory.CreateDirectory(Catename);
                    ObjDirectoryInfo = new DirectoryInfo(Catename);
                    DirectoryInfo ObjSubDirectoryInfo = ObjDirectoryInfo.CreateSubdirectory(SubCatename);
                    loggingDirectoryExists = true;
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SetDocAttributes_Model model = new SetDocAttributes_Model();
                    model.AtrLabel1 = dt.Rows[i][1].ToString();
                    model.AtrType = dt.Rows[i][3].ToString();
                    model.AtrLen = dt.Rows[i][2].ToString();
                    model.AtrMand = dt.Rows[i][4].ToString();
                    model.AtrLovId = Convert.ToInt32(dt.Rows[i][5].ToString());
                    model.UserId = Convert.ToInt32(Session["Emp_Id"].ToString());
                    string aname = model.AtrLabel1.ToString();

                    for (int j = 0; j < dtx.Rows.Count; j++)
                    {
                        if (psotedfilename.ToUpper() == dtx.Rows[j]["File Name"].ToString().ToUpper())
                        {

                            model.AtrText1 = dtx.Rows[j][aname].ToString().Trim();
                            ModelObjList1.Add(model);
                            break;
                        }
                    }
                }
                int Result123 = ServiceObj.SaveProperties(deptsModel, ModelObjList1);
                if (Result123 == 1)
                {
                    string Source = ExistingfilePath;
                    FileInfo objfile = new FileInfo(Source);
                    {
                        string Destination = Path.Combine(Catename, SubCatename);
                        System.IO.File.Move(Source, Destination + "\\" + deptsModel.FileName);
                    }
                }
                return Result123;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return 0;
            }
        }

    }
}