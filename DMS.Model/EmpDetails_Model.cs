﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Model
{
    public class EmpDetails_Model
    {

        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string Grade { get; set; }
        public string EmpTitle { get; set; }
        public string Dept { get; set; }
        public string Unit { get; set; }
        public string EmpDOJ { get; set; }
        public string EmpAddress { get; set; }
        public string City { get; set; }
        public string Pin { get; set; }
        public string State { get; set; }
        public string Region { get; set; }
        public string EmpMobileNo { get; set; }
        public string EmpEmailId { get; set; }
        
       

    }
}
